import React, { Component } from 'react';
import { Button,  StyleSheet, Text, View } from 'react-native';

export default class FriendScreen extends Component {
    render() {
      this.props.navigation.getParam('friend');
      return (
        <View style={styles.container}>
          <Text>Freund</Text>
          <Button title="gehe zu Home" onPress={() => 
            this.props.navigation.goBack()} />
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });