import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';

import { Ionicons } from '@expo/vector-icons';

import HomeScreen from './screens/HomeScreen';
import SettingsScreen from './screens/SettingsScreen';
import FriendScreen from './screens/FriendScreen';


const HomeStack = createStackNavigator({
    HomeScreen,  FriendScreen
}); 

const TabNavigator = createBottomTabNavigator({
  Home: { 
      screen: HomeStack, navigationOptions: { 
            title: 'Freunde',
            tabBarIcon: ({ tintColor }) => {
               return <Ionicons name="ios-home" size={24} color={tintColor} />
            }
         } 
    },
  Settings: { screen: SettingsScreen, navigationOptions: { 
            title: 'Einstellungen',
            tabBarIcon: ({ tintColor }) => {
                return <Ionicons name="ios-settings" size={24} color={tintColor} />
            }
        } 
    }
}, {
    tabBarOptions: {
        activeTintColor: 'darkorange',
        style: {
            backgroundColor: 'aliceblue',
        }
    }
}); 

export default createAppContainer(TabNavigator);
